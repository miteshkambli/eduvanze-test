import {GET_PARTICIPANTS_LIST, GET_PARTICIPANTS_SUCCESS, GET_PARTICIPANTS_FAIL} from "../../actions";


const INITIAL_STATE = {
    error: null,
    data: null,
    loading: true,
};


export default (state = INITIAL_STATE, action) => {
    let {type, payload} = action;
    switch (type) {
        case GET_PARTICIPANTS_LIST:
            return {...state, loading: true, data: null};
        case GET_PARTICIPANTS_SUCCESS:
            return {...state, loading: false, error: '', data: payload};
        case GET_PARTICIPANTS_FAIL:
            return {...state, loading: false, error: payload.error};
        default:
            return state;
    }
};
