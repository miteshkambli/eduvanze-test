import {POST_REGISTER, POST_REGISTER_SUCCESS, POST_REGISTER_FAIL} from "../../actions";


const INITIAL_STATE = {
    error: null,
    data: null,
    loading: true,
};


export default (state = INITIAL_STATE, action) => {
    let {type, payload} = action;
    switch (type) {
        case POST_REGISTER:
            return {...state, loading: true, data: null};
        case POST_REGISTER_SUCCESS:
            return {...state, loading: false, error: '', data: payload};
        case POST_REGISTER_FAIL:
            return {...state, loading: false, error: payload.error};
        default:
            return state;
    }
};
