import {combineReducers} from 'redux';
import DummyReducer from './DummyReducer';
import ListParticipantReducers from './ParticipantsReducers/ListParticipantReducers';

import RegisterReducers from './ParticipantsReducers/RegisterReducers';


export default combineReducers({
  dummy: DummyReducer,
  Register: RegisterReducers,
  ListParticipant: ListParticipantReducers  

});
