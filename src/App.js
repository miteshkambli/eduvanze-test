/**
 * Sample React Native MainRouter
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Provider} from 'react-redux';
import store from './store';
import MainRouter from './routers/MainRouter';

console.disableYellowBox = true;
const App = () => {
  return (
    <Provider store={store}>
          <MainRouter />
    </Provider>
  );
};

export default App;
