import React from 'react';
import styles from './styles';
import {
  Body,
  Container,
  Content,
  Icon,
  ListItem,
  Right,
  Text,
  Item,
  Input,
} from 'native-base';
import { ScrollView, View, ImageBackground } from 'react-native';
import Colors from '../../styles/colorsStyles';

export default class ParticipantsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  renderList() {
    const { data, onPress } = this.props;
    const { searchQuery } = this.state;

    return data.map((item, index) => {
    
        return this.renderItem(item);
      
    });
  }
  renderItem(item) {
    let { user_name, age, locality } = item;
    const { onPress } = this.props;

    return (
      <ListItem onPress={() => onPress(item)}>
        <Body>
          <Text style={styles.name}>{user_name}</Text>
          <Text note style={styles.secondaryTitle}>
            (Age : {age})
          </Text>
          <Text note style={styles.secondaryTitle}>
            (Locality : {locality})
          </Text>
        </Body>

        <Right>
          <View style={styles.rightAlign}>
            <Icon
              name="rightcircle"
              type="AntDesign"
              style={{ color: Colors.black, fontSize: 16 }}
            />
          </View>
        </Right>
      </ListItem>
    );
  }
  render() {
    const { loading, data } = this.props;

    return (
      <View 
        style={styles.container}>
        <Container>
          <View style={styles.topRow}>
            <Text
              style={{
                alignSelf: 'center',
                marginTop: 50,
                alignItems: 'center',
                alignContent:'center',
                alignContent:'center',
                fontFamily: 'bold',
              }}>
               Members
            </Text>

          </View>
          <Content>
            <ScrollView ref={scrollView => (this._scrollView = scrollView)}>
              {data ? this.renderList() : null}
            </ScrollView>
          </Content>
        </Container>
      </View>
    );
  }
}
