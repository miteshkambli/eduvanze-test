import React, { Component } from 'react';
import {
    ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import {
   openParticipateList,
   openChart
  } from './action';
  

import styles from './styles';
import { Content, Icon } from 'native-base';

export default class SideBar extends Component {
    closerDrawer = () => {
        const { drawer } = this.props;
        if (drawer) {
            drawer.close();
        }
    };


    render() {
        const {closeDrawer,goToParticipatesList} = this.props;

        return (
            <Content style={{ backgroundColor: '#FFFFFF' }}>
                <TouchableOpacity
                    style={styles.row}
                    onPress={() => {
                        closeDrawer()
                    }}
                >
                    <Text style={styles.rowTitle}>Registration</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.row}
                    onPress={() => {
                        goToParticipatesList()
                        closeDrawer()
                    }}
                >
                    <Text style={styles.rowTitle}>Participants List</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.row}
                    onPress={() => {
                        closeDrawer()
                    }}
                >
                    <Text style={styles.rowTitle}>Chart</Text>
                </TouchableOpacity>
                

            </Content>
        );
    }
}