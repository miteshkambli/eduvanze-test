import React, {Component} from 'react';
import {Text} from 'react-native';
import {
  Header,
  Left,
  Button,
  Icon,
  Right,
  Body,
  Title,
  Drawer,
} from 'native-base';
import SideBar from '../../components/Slidebar';
import DashboardComponent from '../../components/DashboardComponent';
import {postRegister} from '../../actions';
import {connect} from 'react-redux';
class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  closeDrawer() {
    this.drawer && this.drawer._root && this.drawer._root.close();
  }
  openDrawer() {
    this.drawer && this.drawer._root && this.drawer._root.open();
  }
  submit(data) {
    this.props.Register(data)
  }
  goToParticipatesList(){
    this.props.navigation.navigate('ParticipantsList');
  }
  render() {
    const closeDrawer = this.closeDrawer.bind(this);
    const goToParticipatesList = this.goToParticipatesList.bind(this);
    const submit = this.submit.bind(this);
    return (
      <Drawer
        ref={(ref) => {
          this.drawer = ref;
        }}
        content={<SideBar closeDrawer={closeDrawer} goToParticipatesList={goToParticipatesList} />}
        onClose={() => this.closeDrawer()}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.openDrawer()}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body></Body>
        </Header>
        <DashboardComponent submit={submit} />
      </Drawer>
    );
  }
}
// module.exports = AppHeader
const mapStateToProps = (state) => ({
  Register: state.Register,
});

export default connect(mapStateToProps, {
  postRegister,
})(AppHeader);
