import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadParticipantsData} from '../../actions';
import ParticipantsComponent from '../../components/ParticipantsComponent';

class ParticipantsListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isInternetConnected: true,
    };
  }

  static navigationOptions = {
    title: 'Participants list',
    headerRight: null,
  };

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    this.props.loadParticipantsData();
  }

  // goToDetail(user) {
  //     this.props.navigation.navigate('ParticipantsDetail', {user});
  // }

  render() {
    const {data, loading} = this.props.ListParticipant;

    return <ParticipantsComponent data={data} />;
  }
}

const mapStateToProps = (state) => ({
  ListParticipant: state.ListParticipant,
});

export default connect(mapStateToProps, {loadParticipantsData})(
  ParticipantsListContainer,
);
