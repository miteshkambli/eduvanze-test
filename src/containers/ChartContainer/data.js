// Mock data object used for LineChart and BarChart

const data = {
    labels: ['12-18', '18-25', '25-30', '30+'],
    datasets: [{
      data: [
        50,
        20,
        2,
        86,
        71,
        100
      ],
      color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})` // optional
    },{
      data: [
        20,
        10,
        4,
        56,
        87,
        90
      ]
    },{
      data: [
        30,
        90,
        67,
        54,
        10,
        2
      ]
    }]
  }

  // Mock data object used for Contribution Graph



  // Mock data object for Pie Chart

  const pieChartData = [
    { name: 'Mumbai', population: 21500000, color: 'rgba(131, 167, 234, 1)', legendFontColor: '#7F7F7F', legendFontSize: 15 },
    { name: 'Navi Mumvbai', population: 2800000, color: '#F00', legendFontColor: '#7F7F7F', legendFontSize: 15 },
    { name: 'Mumbai', population: 527612, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 15 },
    { name: 'Thane', population: 11920000, color: 'rgb(0, 0, 255)', legendFontColor: '#7F7F7F', legendFontSize: 15 }
  ]

  // Mock data object for Progress

  const progressChartData = [0.1, 0.0, 0.2]

  export { data, pieChartData, progressChartData }