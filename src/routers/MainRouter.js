import React, {Component} from 'react';
import SwitchNavigator from './SwitchNavigator';
import {Root} from 'native-base';
import {StatusBar} from 'react-native';

export default class MainRouter extends Component {
  render() {
    return (
      <Root>
        <StatusBar hidden={true} />
        <SwitchNavigator />
      </Root>
    );
  }
}
