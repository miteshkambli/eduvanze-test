import React, {Fragment} from 'react';
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';

import Dashboard from "../containers/DashboardContainer";
import ParticipantsListContainer from "../containers/ParticipantsListContainer";
import ChartContainer from "../containers/ChartContainer";

const AppStack = createStackNavigator({   
    ParticipantsList : ParticipantsListContainer,
    ChartContainer: ChartContainer
});



const AppNavigator = createSwitchNavigator(
    {   Dashboard,
        App: AppStack
    },
    {
        initialRouteName: "Dashboard"
    }
);

export default createAppContainer(AppNavigator);
